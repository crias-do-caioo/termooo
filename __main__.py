
from random import choice
from termo.termo import Termo, Feedback, Result, InvalidAttempt
# pip install --user termcolor
# from termcolor import colored

# pip install --user coverage
# pip install unidecode
# coverage run -m unittest
# coverage report
# coverage html

def main():
    words = ['adoro', 'anota', 'brota', 'chapo', 'copio', 'elege', 'ferve', 'leigo', 'mirou', 'notem', 'otita', 'ramal', 'sugou', 'turbo']
    word = choice(words)
    limit = 5
    counter = 0
    termo = Termo(word, words)
    result = Result(win=False, feedback=None)

    print('Tente adivinhar a palavra')

    while not result.win and counter < limit:
        try:
            guess = input(f'Tentativa: ')
            result = termo.test(guess)
            counter += 1
            print(result)
        except InvalidAttempt:
            print('Tente de novo')
    if result.win:
        print('Parabéns')
    else:
        print(word)
        print('Game over')


if __name__ == '__main__':
    main()
