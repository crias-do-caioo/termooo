from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('termo.urls')),
    path('api/', include('termo.urls')),
]