import os
import django
from datetime import timedelta, datetime
from random import shuffle

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
django.setup()

from termo.models import Pool, Word

# Lista de palavras a serem adicionadas
words = ['adoro', 'anota', 'brota', 'chapo', 'copio', 'elege', 'ferve', 'leigo', 'mirou', 'notem', 'otita', 'ramal', 'sugou', 'turbo']
shuffle(words)

# Pegar dia atual com hora setada para 00:00
diaAtual = datetime.combine(datetime.today(), datetime.min.time())

# Adiciona cada palavra ao modelo Word
for i, word in enumerate(words):
    createdWord, _ = Word.objects.get_or_create(content=word)

    if not _:
        print(f'Palavra {word} já existe, pulando...')
        continue

    Pool.objects.create(
            word = createdWord,
            datetime = diaAtual + timedelta(days=i),
        )

print("Palavras adicionadas com sucesso!")