import unittest
from django.utils import timezone
from django.test import TestCase
from termo.models import get_word_of_day
from datetime import timedelta

# Create your tests here.


class WordTest(TestCase):

    def test_get_word_of_day_is_not_none(self):
        now = timezone.now()
        a = get_word_of_day()
        self.assertIsNotNone(a)

    def test_is_the_same_word(self):
        now = timezone.now()
        a = get_word_of_day()
        b = get_word_of_day()
        self.assertEqual(a, b)
