from rest_framework import serializers
from .models import Pool, Word

class WordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Word
        fields = ['content']

class PoolSerializer(serializers.ModelSerializer):
    word = WordSerializer()

    class Meta:
        model = Pool
        fields = ['datetime', 'word']