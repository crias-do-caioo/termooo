import json

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Pool, get_word_of_day
from .serializers import PoolSerializer
from datetime import datetime
from core.TermoBackend import Termo, Feedback, Result, InvalidAttempt

f = open('core/palavras.json')
words = json.load(f)
f.close()

class WordOfDayView(APIView):
    def get(self, request, format=None):
        date = datetime.combine(datetime.today(), datetime.min.time())

        word_of_day = get_word_of_day()

        if not word_of_day:
            return Response('Palavra não encontrada para o dia atual', status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        serializer = PoolSerializer(word_of_day)

        return Response(serializer.data, status=status.HTTP_200_OK)

class VerifyWordView(APIView):
    def get(self, request):
        palavra = request.GET.get('p')

        word = get_word_of_day().word.content
        termo = Termo(word, words)
        result = Result(win=False, feedback=None)

        try:
            result = termo.test(palavra)
            res_txt = result
        except InvalidAttempt:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        return Response(res_txt, status=status.HTTP_200_OK)