from django.db import models
from django.conf import settings
from datetime import timedelta, datetime

class Word(models.Model):
    content = models.CharField(
        max_length=5,
    )
            
    def __str__(self):
        return self.content

class Pool(models.Model):
    datetime = models.DateTimeField(   
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        null=True,
    )

    word = models.ForeignKey(
        Word,
        on_delete=models.CASCADE,
    )

def get_word_of_day():
    try:
        return Pool.objects.filter(
            datetime__lte = datetime.combine(datetime.today(), datetime.min.time()),
        ).get()
    except:
        return ''