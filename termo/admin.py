from django.contrib import admin
from .models import Word, Pool

@admin.register(Word)
class WordAdmin(admin.ModelAdmin):
    list_display = ['content']

@admin.register(Pool)
class PoolsAdmin(admin.ModelAdmin):
    list_display = ['word', 'datetime', 'user']