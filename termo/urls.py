from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
from django.shortcuts import render
from .views import WordOfDayView, VerifyWordView

app_name = 'termo'

urlpatterns = [
    path('', lambda request: render(request, 'index.html')),
    path('info/', lambda request: render(request, 'info.html')),
    path('rule/', lambda request: render(request, 'rule.html')),
    path('word-of-day/', WordOfDayView.as_view(), name='word-of-day'),
    path('verify', VerifyWordView.as_view(), name='verify'),
]