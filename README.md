# Termooo

## Projeto réplica do game term.ooo para a matéria de engenharia de software - UNIMAR.

### TODO LIST

- [x] Trocar o verde do /info;
- [x] Arrumar todos os links /info;
- [x] Tirar o Trelo do /info;
- [x] Deixar o jogo funcional no frontend;
- [x] Arrumar o CSS do popup stats;
- [x] Arrumar o relógio do popup stats;
- [x] Arrumar a cor do popup de config;
- [ ] Deixar as opções do popup de config funcionais;
- [x] Adicionar o favicon;
- [x] Deixar o jogo funcional no backend;
- [ ] Colorir o teclado de acordo com as tentativas
- [ ] Arrumar a animação dos popups do index
- [ ] Salvar tentativas através dos cookies
- [ ] Mostar popup no fim do jogo